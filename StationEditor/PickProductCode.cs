﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProductDatabaseDLL;


namespace StationEditor
{
    public partial class PickProductCode : Form
    {
        List<ProductDatabase.Product_data> productDataList = new List<ProductDatabase.Product_data>();
        public ProductDatabase.Product_data productData = new ProductDatabase.Product_data();

        public PickProductCode(List<ProductDatabase.Product_data> pdata)
        {
            InitializeComponent();
            productDataList = pdata;
            lvStationData.ColumnClick += new ColumnClickEventHandler(ColumnClick);
        }

        // ColumnClick event handler.
        private void ColumnClick(object o, ColumnClickEventArgs e)
        {
            // Set the ListViewItemSorter property to a new ListViewItemComparer 
            // object. Setting this property immediately sorts the 
            // ListView using the ListViewItemComparer object.
            this.lvStationData.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void PickProductCode_Load(object sender, EventArgs e)
        {
            string msg = string.Empty;

            foreach (var item in productDataList)
            {
                ListViewItem oneItem = new ListViewItem(item.product_code);
                oneItem.SubItems.Add(item.limit_version);
                oneItem.SubItems.Add(item.active_part ? "YES" : "NO");
                oneItem.SubItems.Add(item.product_code_modify_date.ToString());
                lvStationData.Items.Add(oneItem);
            }

        }
        private void lvStationData_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvStationData.SelectedIndices.Count == 0)      // if nothing selected
                return;

            for (int i = 0; i < productDataList.Count; i++)
            {
                if (lvStationData.SelectedItems[0].Text == productDataList[i].product_code)
                {
                    productData = (ProductDatabase.Product_data)productDataList[i].Clone();
                }
            }
            DialogResult = DialogResult.OK;
        }

    }

}
