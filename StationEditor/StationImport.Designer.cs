﻿namespace StationEditor
{
    partial class StationImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lImportingTo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bStartImport = new System.Windows.Forms.Button();
            this.tbFile = new System.Windows.Forms.TextBox();
            this.bDone = new System.Windows.Forms.Button();
            this.bSelectFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // lImportingTo
            // 
            this.lImportingTo.AutoSize = true;
            this.lImportingTo.Location = new System.Drawing.Point(48, 44);
            this.lImportingTo.Name = "lImportingTo";
            this.lImportingTo.Size = new System.Drawing.Size(19, 13);
            this.lImportingTo.TabIndex = 11;
            this.lImportingTo.Text = "----";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Importing to";
            // 
            // bStartImport
            // 
            this.bStartImport.Location = new System.Drawing.Point(27, 162);
            this.bStartImport.Name = "bStartImport";
            this.bStartImport.Size = new System.Drawing.Size(75, 23);
            this.bStartImport.TabIndex = 9;
            this.bStartImport.Text = "Start Import";
            this.bStartImport.UseVisualStyleBackColor = true;
            this.bStartImport.Click += new System.EventHandler(this.bStartImport_Click);
            // 
            // tbFile
            // 
            this.tbFile.Location = new System.Drawing.Point(27, 118);
            this.tbFile.Name = "tbFile";
            this.tbFile.Size = new System.Drawing.Size(464, 20);
            this.tbFile.TabIndex = 8;
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(81, 191);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 7;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            // 
            // bSelectFile
            // 
            this.bSelectFile.Location = new System.Drawing.Point(27, 89);
            this.bSelectFile.Name = "bSelectFile";
            this.bSelectFile.Size = new System.Drawing.Size(155, 23);
            this.bSelectFile.TabIndex = 6;
            this.bSelectFile.Text = "Select import file";
            this.bSelectFile.UseVisualStyleBackColor = true;
            this.bSelectFile.Click += new System.EventHandler(this.SelectFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // StationImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 246);
            this.Controls.Add(this.lImportingTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bStartImport);
            this.Controls.Add(this.tbFile);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.bSelectFile);
            this.Name = "StationImport";
            this.Text = "StationImport";
            this.Load += new System.EventHandler(this.StationImport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lImportingTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bStartImport;
        private System.Windows.Forms.TextBox tbFile;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.Button bSelectFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}