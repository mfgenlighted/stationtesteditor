﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StationEditor
{
    public partial class ServerSetup : Form
    {
        public ServerSetup()
        {
            InitializeComponent();
        }

        private void ServerSetup_Load(object sender, EventArgs e)
        {
            tbS1Name.Text = Properties.Settings.Default.S1Name;
            tbS1IP.Text = Properties.Settings.Default.S1IP;
            tbS1User.Text = Properties.Settings.Default.S1User;
            tbS1Password.Text = Properties.Settings.Default.S1Password;
            tbS1Database.Text = Properties.Settings.Default.S1Database;
            tbS2Name.Text = Properties.Settings.Default.S2Name;
            tbS2IP.Text = Properties.Settings.Default.S2IP;
            tbS2User.Text = Properties.Settings.Default.S2User;
            tbS2Password.Text = Properties.Settings.Default.S2Password;
            tbS2Database.Text = Properties.Settings.Default.S2Database;
            tbS3Name.Text = Properties.Settings.Default.S3Name;
            tbS3IP.Text = Properties.Settings.Default.S3IP;
            tbS3User.Text = Properties.Settings.Default.S3User;
            tbS3Password.Text = Properties.Settings.Default.S3Password;
            tbS3Database.Text = Properties.Settings.Default.S3Database;
            tbS4Name.Text = Properties.Settings.Default.S4Name;
            tbS4IP.Text = Properties.Settings.Default.S4IP;
            tbS4User.Text = Properties.Settings.Default.S4User;
            tbS4Password.Text = Properties.Settings.Default.S4Password;
            tbS4Database.Text = Properties.Settings.Default.S4Database;
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.S1Name = tbS1Name.Text;
            Properties.Settings.Default.S1IP = tbS1IP.Text;
            Properties.Settings.Default.S1User = tbS1User.Text;
            Properties.Settings.Default.S1Password = tbS1Password.Text;
            Properties.Settings.Default.S1Database = tbS1Database.Text;
            Properties.Settings.Default.S2Name = tbS2Name.Text;
            Properties.Settings.Default.S2IP = tbS2IP.Text;
            Properties.Settings.Default.S2User = tbS2User.Text;
            Properties.Settings.Default.S2Password = tbS2Password.Text;
            Properties.Settings.Default.S2Database = tbS2Database.Text;
            Properties.Settings.Default.S3Name = tbS3Name.Text;
            Properties.Settings.Default.S3IP = tbS3IP.Text;
            Properties.Settings.Default.S3User = tbS3User.Text;
            Properties.Settings.Default.S3Password = tbS3Password.Text;
            Properties.Settings.Default.S3Database = tbS3Database.Text;
            Properties.Settings.Default.S4Name = tbS4Name.Text;
            Properties.Settings.Default.S4IP = tbS4IP.Text;
            Properties.Settings.Default.S4User = tbS4User.Text;
            Properties.Settings.Default.S4Password = tbS4Password.Text;
            Properties.Settings.Default.S4Database = tbS4Database.Text;
            Properties.Settings.Default.Save();
        }
    }
}
