﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Collections;
using System.Drawing;

using ProductDatabaseDLL;

namespace StationEditor
{
    public struct serverDataItem
    {
        public string name;
        public string ip;
        public string user;
        public string password;
        public string database;
    }


    public partial class Form1 : Form
    {
        public List<ProductDatabase.Station_data> stations;

        serverDataItem selectedServerData = new serverDataItem();

        public Form1()
        {
            InitializeComponent();
            lvStations.ColumnClick += new ColumnClickEventHandler(ColumnClick);
        }


        // ColumnClick event handler.
        private void ColumnClick(object o, ColumnClickEventArgs e)
        {
            // Set the ListViewItemSorter property to a new ListViewItemComparer 
            // object. Setting this property immediately sorts the 
            // ListView using the ListViewItemComparer object.
            this.lvStations.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void serversToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServerSetup iform = new StationEditor.ServerSetup();
            iform.ShowDialog();
            iform.Dispose();
        }

        private void bReadStationData_Click(object sender, EventArgs e)
        {
            stations = new List<ProductDatabase.Station_data>();

            SelectIServerToReadStationFrom iform = new StationEditor.SelectIServerToReadStationFrom();
            iform.ShowDialog();
            lbSelectedServer.Text = iform.SelectedServer;
            stations = iform.stationList;
            selectedServerData = iform.selectedServer;
            iform.Dispose();

            lvStations.Items.Clear();
            foreach (var item in stations)
            {
                ListViewItem onelvLimit = new ListViewItem(item.name);
                onelvLimit.SubItems.Add(item.function);
                onelvLimit.SubItems.Add(item.active ? "YES":"NO");
                onelvLimit.SubItems.Add(item.cm_code);
                lvStations.Items.Add(onelvLimit);
            }

            DialogResult = DialogResult.OK;
        }



        private void bEditProductCodes_Click(object sender, EventArgs e)
        {
            SelsectedStationList iform = null;
            if (lvStations.SelectedIndices.Count == 0)      // if nothing selected
                return;

            for(int i = 0; i < stations.Count; i++)
            {
                if (lvStations.SelectedItems[0].Text == stations[i].name)
                {
                    iform = new SelsectedStationList(stations[i], selectedServerData);
                }
            }

            iform.ShowDialog();
            iform.Dispose();

        }

        /// <summary>
        /// Adds a new station to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAddNewStation_Click(object sender, EventArgs e)
        {
            ProductDatabase.Station_data stationdata = new ProductDatabase.Station_data();
            EditStationEntry iform = new StationEditor.EditStationEntry(stationdata);
            Guid newguid = Guid.Empty;
            string message = string.Empty;
            int errorcode = 0;
            DialogResult editresponse;

            iform.ShowDialog();
            stationdata = iform.stationdata;
            editresponse = iform.DialogResult;
            iform.Dispose();

            if (editresponse == DialogResult.Cancel)    // if the edit was canceled
                return;

            stations.Add(stationdata);              // add new data to list

            ProductDatabase prodDB = new ProductDatabase();
            if (!prodDB.Open(selectedServerData.ip, selectedServerData.database, selectedServerData.user, selectedServerData.password))
            {
                MessageBox.Show("Error opening the database. ");
                return;
            }

            if ((newguid = prodDB.AddStationData(stationdata)) == Guid.Empty)       // if there was a error
            {
                errorcode = prodDB.CheckForErrors(out message);
                MessageBox.Show("Error saving data. Data not saved. Error code = " + errorcode + " " + message);
                return;
            }

            lvStations.Items.Clear();
            foreach (var item in stations)
            {
                ListViewItem onelvLimit = new ListViewItem(item.name);
                onelvLimit.SubItems.Add(item.function);
                onelvLimit.SubItems.Add(item.active ? "YES" : "NO");
                onelvLimit.SubItems.Add(item.cm_code);
                lvStations.Items.Add(onelvLimit);
            }

        }

        /// <summary>
        /// Edit a existing station.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bEditStationData_Click(object sender, EventArgs e)
        {
            ProductDatabase.Station_data stationdata = new ProductDatabase.Station_data();
            EditStationEntry iform;
            Guid newguid = Guid.Empty;
            string message = string.Empty;
            int errorcode = 0;

            if (lvStations.SelectedIndices.Count == 0)      // if nothing is selected
            {
                MessageBox.Show("No station is selected.");
                return;
            }

            iform = new StationEditor.EditStationEntry(stations[lvStations.SelectedIndices[0]]);

            iform.ShowDialog();
            stationdata = iform.stationdata;
            iform.Dispose();

            ProductDatabase prodDB = new ProductDatabase();
            if (!prodDB.Open(selectedServerData.ip, selectedServerData.database, selectedServerData.user, selectedServerData.password))
            {
                MessageBox.Show("Error opening the database. ");
                return;
            }


            if (!prodDB.ReplaceStationData(stationdata.station_id, stationdata))       // if there was a error
            {
                errorcode = prodDB.CheckForErrors(out message);
                MessageBox.Show("Error saving data. Data not saved. Error code = " + errorcode + " " + message);
                return;
            }

            stations[lvStations.SelectedIndices[0]] = stationdata;

            lvStations.Items.Clear();
            foreach (var item in stations)
            {
                ListViewItem onelvLimit = new ListViewItem(item.name);
                onelvLimit.SubItems.Add(item.function);
                onelvLimit.SubItems.Add(item.active ? "YES" : "NO");
                onelvLimit.SubItems.Add(item.cm_code);
                lvStations.Items.Add(onelvLimit);
            }

        }

        private void bDeleteStation_Click(object sender, EventArgs e)
        {
            int errorcode = 0;
            string message = string.Empty;
            Guid deleteguid = Guid.Empty;

            if (lvStations.SelectedIndices.Count == 0)      // if nothing is selected
            {
                MessageBox.Show("No station is selected.");
                return;
            }
            if (MessageBox.Show("Are you sure you want to delete this? It will delete\nany product code attached to it also.", "Delete a station", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            ProductDatabase prodDB = new ProductDatabase();
            if (!prodDB.Open(selectedServerData.ip, selectedServerData.database, selectedServerData.user, selectedServerData.password))
            {
                MessageBox.Show("Error opening the database. ");
                return;
            }


            if (!prodDB.ReplaceStationData(stations[lvStations.SelectedIndices[0]].station_id, null))       // if there was a error
            {
                errorcode = prodDB.CheckForErrors(out message);
                MessageBox.Show("Error deleting data. Error code = " + errorcode + " " + message);
                return;
            }

            stations.RemoveAt(lvStations.SelectedIndices[0]);

            lvStations.Items.Clear();
            foreach (var item in stations)
            {
                    ListViewItem onelvLimit = new ListViewItem(item.name);
                    onelvLimit.SubItems.Add(item.function);
                    onelvLimit.SubItems.Add(item.active ? "YES" : "NO");
                onelvLimit.SubItems.Add(item.cm_code);
                    lvStations.Items.Add(onelvLimit);
            }

        }

        private void bExportStationData_Click(object sender, EventArgs e)
        {
            StationExport iform = new StationEditor.StationExport();
            iform.ShowDialog();
            iform.Dispose();
        }

        private void bReplaceData_Click(object sender, EventArgs e)
        {
            StationImport iform = new StationEditor.StationImport();
            iform.ShowDialog();
            iform.Dispose();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string textProgramVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            MessageBox.Show("Program version: " + textProgramVersion);
        }

    }

    // Implements the manual sorting of items by columns.
    class ListViewItemComparer : IComparer
    {
        private int col;
        public ListViewItemComparer()
        {
            col = 0;
        }
        public ListViewItemComparer(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
        }
    }

}
