﻿namespace StationEditor
{
    partial class PickProductCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvStationData = new System.Windows.Forms.ListView();
            this.chProductCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbLimitVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chActive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chModifyDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvStationData
            // 
            this.lvStationData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chProductCode,
            this.cbLimitVersion,
            this.chActive,
            this.chModifyDate});
            this.lvStationData.Location = new System.Drawing.Point(23, 30);
            this.lvStationData.Name = "lvStationData";
            this.lvStationData.Size = new System.Drawing.Size(431, 566);
            this.lvStationData.TabIndex = 1;
            this.lvStationData.UseCompatibleStateImageBehavior = false;
            this.lvStationData.View = System.Windows.Forms.View.Details;
            this.lvStationData.SelectedIndexChanged += new System.EventHandler(this.lvStationData_SelectedIndexChanged);

            // 
            // chProductCode
            // 
            this.chProductCode.Text = "Product Code";
            this.chProductCode.Width = 87;
            // 
            // cbLimitVersion
            // 
            this.cbLimitVersion.Text = "Limit Version";
            this.cbLimitVersion.Width = 84;
            // 
            // chActive
            // 
            this.chActive.Text = "Active";
            // 
            // chModifyDate
            // 
            this.chModifyDate.Text = "ModifyDate";
            this.chModifyDate.Width = 97;
            // 
            // PickProductCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 608);
            this.Controls.Add(this.lvStationData);
            this.Name = "PickProductCode";
            this.Text = "PickProductCode";
            this.Load += new System.EventHandler(this.PickProductCode_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvStationData;
        private System.Windows.Forms.ColumnHeader chProductCode;
        private System.Windows.Forms.ColumnHeader cbLimitVersion;
        private System.Windows.Forms.ColumnHeader chActive;
        private System.Windows.Forms.ColumnHeader chModifyDate;
    }
}