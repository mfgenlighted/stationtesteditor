﻿namespace StationEditor
{
    partial class SelectServerToOpen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bServer1 = new System.Windows.Forms.Button();
            this.bServer3 = new System.Windows.Forms.Button();
            this.bServer2 = new System.Windows.Forms.Button();
            this.bServer4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bServer1
            // 
            this.bServer1.Location = new System.Drawing.Point(24, 37);
            this.bServer1.Name = "bServer1";
            this.bServer1.Size = new System.Drawing.Size(210, 23);
            this.bServer1.TabIndex = 0;
            this.bServer1.Text = "button1";
            this.bServer1.UseVisualStyleBackColor = true;
            this.bServer1.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer3
            // 
            this.bServer3.Location = new System.Drawing.Point(24, 98);
            this.bServer3.Name = "bServer3";
            this.bServer3.Size = new System.Drawing.Size(210, 23);
            this.bServer3.TabIndex = 1;
            this.bServer3.Text = "button2";
            this.bServer3.UseVisualStyleBackColor = true;
            this.bServer3.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer2
            // 
            this.bServer2.Location = new System.Drawing.Point(24, 69);
            this.bServer2.Name = "bServer2";
            this.bServer2.Size = new System.Drawing.Size(210, 23);
            this.bServer2.TabIndex = 2;
            this.bServer2.Text = "button3";
            this.bServer2.UseVisualStyleBackColor = true;
            this.bServer2.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer4
            // 
            this.bServer4.Location = new System.Drawing.Point(24, 127);
            this.bServer4.Name = "bServer4";
            this.bServer4.Size = new System.Drawing.Size(210, 23);
            this.bServer4.TabIndex = 3;
            this.bServer4.Text = "button4";
            this.bServer4.UseVisualStyleBackColor = true;
            this.bServer4.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // SelectServerToOpen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 190);
            this.ControlBox = false;
            this.Controls.Add(this.bServer4);
            this.Controls.Add(this.bServer2);
            this.Controls.Add(this.bServer3);
            this.Controls.Add(this.bServer1);
            this.Name = "SelectServerToOpen";
            this.Text = "Select server to open";
            this.Load += new System.EventHandler(this.SelectToServer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bServer1;
        private System.Windows.Forms.Button bServer3;
        private System.Windows.Forms.Button bServer2;
        private System.Windows.Forms.Button bServer4;
    }
}