﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

using ProductDatabaseDLL;

namespace StationEditor
{
    public partial class SelectServerToOpen : Form
    {
        public string SelectedServer = string.Empty;
        public ProductDatabase.Product_data stationdata = new ProductDatabase.Product_data();
        public List<ProductDatabase.Station_data> stationList = new List<ProductDatabase.Station_data>();
        public serverDataItem selectedServer = new serverDataItem();


        private List<serverDataItem> allserverData = new List<serverDataItem>();

        public SelectServerToOpen()
        {
            InitializeComponent();
        }

        //------------------------------
        // button events
        
        private void SelectToServer_Load(object sender, EventArgs e)
        {
            bServer1.Text = Properties.Settings.Default.S1Name;
            bServer2.Text = Properties.Settings.Default.S2Name;
            bServer3.Text = Properties.Settings.Default.S3Name;
            bServer4.Text = Properties.Settings.Default.S4Name;

            serverDataItem item = new serverDataItem();
            item.name = Properties.Settings.Default.S1Name;
            item.ip = Properties.Settings.Default.S1IP;
            item.user = Properties.Settings.Default.S1User;
            item.password = Properties.Settings.Default.S1Password;
            item.database = Properties.Settings.Default.S1Database;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S2Name;
            item.ip = Properties.Settings.Default.S2IP;
            item.user = Properties.Settings.Default.S2User;
            item.password = Properties.Settings.Default.S2Password;
            item.database = Properties.Settings.Default.S2Database;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S3Name;
            item.ip = Properties.Settings.Default.S3IP;
            item.user = Properties.Settings.Default.S3User;
            item.password = Properties.Settings.Default.S3Password;
            item.database = Properties.Settings.Default.S3Database;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S4Name;
            item.ip = Properties.Settings.Default.S4IP;
            item.user = Properties.Settings.Default.S4User;
            item.password = Properties.Settings.Default.S4Password;
            item.database = Properties.Settings.Default.S4Database;
            allserverData.Add(item);

        }

        private void bServer1_Click(object sender, EventArgs e)
        {
            serverDataItem item = new serverDataItem();
            ProductDatabase prodDB = new ProductDatabase();
            string msg = string.Empty;

            Button clickedbutton = (Button)sender;
            SelectedServer = clickedbutton.Text;

            item = allserverData.Find(x => x.name == clickedbutton.Text);
            selectedServer = item;
            if (prodDB.Open(item.ip, item.database, item.user, item.password))
               prodDB.GetListOfStations(ref stationList, out msg);
            else
            {
                MessageBox.Show("Error opening database. " + prodDB.LastErrorMessage);
            }

            DialogResult = DialogResult.OK;

        }

    }
}
