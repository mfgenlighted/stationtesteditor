﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProductDatabaseDLL;

namespace StationEditor
{
    public partial class EditProductCodeEntry : Form
    {
        public ProductDatabase.Station_data stationData = new ProductDatabase.Station_data();
        public ProductDatabase.Product_data productData = new ProductDatabase.Product_data();

        public EditProductCodeEntry(ProductDatabase.Station_data sdata, ProductDatabase.Product_data pdata)
        {
            stationData = sdata;
            productData = (ProductDatabase.Product_data)pdata.Clone();      // just make a copy
            InitializeComponent();
        }

        private void EditStationEntry_Load(object sender, EventArgs e)
        {
            lbStation.Text = stationData.name;
            lbFunction.Text = stationData.function;
            tbHLAProductCode.Text = productData.product_code;
            tbPCBAProductCode.Text = productData.pcba_product_code;
            tbModel.Text = productData.model;
            tbHLAPN.Text = productData.pn;
            tbPCBAPN.Text = productData.pcba_used;
            tbFamilyCode.Text = productData.family_code;
            nudNumberOfMacs.Value = productData.number_of_macs;
            tbFirmwareVersion.Text = productData.firmware_version;
            tbFirmwareID.Text = productData.firmware_id;
            nudHwCode.Value = productData.hw_code;
            tbProductDesc.Text = productData.product_desc;
            tbLimitVersion.Text = productData.limit_version;
            cbActivePart.Checked = productData.active_part;
            tbPFSAssemblyNumber.Text = productData.pfs_assembly_number;
            tbECO.Text = productData.eco;
            productData.product_code_modify_date = DateTime.Now;
            tbModifyDate.Text = productData.product_code_modify_date.ToString();
            tbComment.Text = productData.product_code_comment;
            tbCMcodes.Text = productData.cm_codes;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            productData.product_code = tbHLAProductCode.Text;
            productData.pcba_product_code = tbPCBAProductCode.Text;
            productData.model = tbModel.Text;
            productData.pn = tbHLAPN.Text;
            productData.pcba_used = tbPCBAPN.Text;
            productData.family_code = tbFamilyCode.Text;
            productData.product_desc = tbProductDesc.Text;
            productData.limit_version = tbLimitVersion.Text;
            productData.active_part = cbActivePart.Checked;
            productData.pfs_assembly_number = tbPFSAssemblyNumber.Text;
            productData.eco = tbECO.Text;
            productData.product_code_modify_date = DateTime.Parse(tbModifyDate.Text);
            productData.product_code_comment = tbComment.Text;
            productData.cm_codes = tbCMcodes.Text;
            productData.hw_code = (int)nudHwCode.Value;
            productData.number_of_macs = (int)nudNumberOfMacs.Value;
            productData.firmware_id = tbFirmwareID.Text;
            productData.firmware_version = tbFirmwareVersion.Text;

            DialogResult = DialogResult.OK;
        }

        private void nudHwCode_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            MessageBox.Show("Hardware code help");
        }
    }
}
