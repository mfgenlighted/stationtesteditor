﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using ProductDatabaseDLL;

namespace StationEditor
{
    public partial class StationExport : Form
    {
        serverDataItem serverdata = new serverDataItem();
        List<ProductDatabase.Station_data> stationList;
        List<ProductDatabase.Product_data> productList;
        ProductDatabaseDLL.ProductDatabase prodDB = new ProductDatabase();

        public StationExport()
        {
            InitializeComponent();
        }

        //------------ Utilitys
        private static int ExecuteCommand(string commnd, int timeout, ref string outputString, ref string errorString)
        {
            var pp = new ProcessStartInfo("cmd.exe", "/c " + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = @"C:\Program Files\MySQL\MySQL Server 8.0\bin",
            };
            pp.RedirectStandardError = true;
            pp.RedirectStandardOutput = true;
            var process = Process.Start(pp);
            try
            {
                process.WaitForExit(timeout);
            }
            catch (Exception)
            {
                return -1;
            }
            using (StreamReader rdr = process.StandardOutput)
            {
                outputString = rdr.ReadToEnd();
            }
            using (StreamReader rdr = process.StandardError)
            {
                errorString = rdr.ReadToEnd();
            }
            process.Close();
            return 0;
        }

        //------------ events
        private void button1_Click(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            string outputString = string.Empty;
            string errorString = string.Empty;

            Color oldcolor;

            if (lvAllStations.SelectedItems.Count == 0)
            {
                MessageBox.Show("Must select a Statopm to export.");
                return;
            }

            prodDB.GetStationProductData(lvAllStations.SelectedItems[0].Text, out productList, out errorString);
            oldcolor = bExport.BackColor;
            bExport.BackColor = Color.Green;
            bExport.Text = "WORKING";

            bExport.BackColor = oldcolor;
            bExport.Text = "Export";
        }

        private void StationExport_Load(object sender, EventArgs e)
        {
            stationList = new List<ProductDatabase.Station_data>();

            SelectServerToOpen iform = new StationEditor.SelectServerToOpen();
            iform.ShowDialog();
            lbExportingFrom.Text = iform.SelectedServer;
            stationList = iform.stationList;
            serverdata = iform.selectedServer;
            iform.Dispose();

            lvAllStations.Items.Clear();
            foreach (var item in stationList)
            {
                ListViewItem onelvLimit = new ListViewItem(item.name);
                onelvLimit.SubItems.Add(item.function);
                onelvLimit.SubItems.Add(item.modify_date.ToString());
                onelvLimit.SubItems.Add(item.active ? "YES" : "NO");
                lvAllStations.Items.Add(onelvLimit);
            }

            tbDirectory.Text = Properties.Settings.Default.SQLDirectory;
        }

        private void bExport_Click(object sender, EventArgs e)
        {
            int status = 0;
            string dumpfilename = string.Empty;
            string cmd = string.Empty;
            string outputString = string.Empty;
            string errorString = string.Empty;
            List<ProductDatabase.Product_data> productList = new List<ProductDatabase.Product_data>();
            ProductDatabase prodDB = new ProductDatabase();

            Color oldcolor;
            string oldtext;

            if (lvAllStations.SelectedItems.Count == 0)
            {
                MessageBox.Show("Must select a station to export.");
                return;
            }
            if (tbDirectory.Text == string.Empty)
            {
                MessageBox.Show("Must set a directory.");
                return;
            }
            oldcolor = bExport.BackColor;
            oldtext = bExport.Text;

            bExport.BackColor = Color.Green;
            bExport.Text = "Working";
            dumpfilename = "\"" + Properties.Settings.Default.SQLDirectory + "\\" + stationList[lvAllStations.SelectedIndices[0]].name + "_" + DateTime.Now.ToString("MM-dd-yyyyTHH_mm") + ".sql\"";
            if (File.Exists(dumpfilename))
            {
                if (MessageBox.Show("File exists with the name " + dumpfilename + ". Do you want to overwrite? No will exit.", "File Exists", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    File.Delete(dumpfilename);
                else
                {
                    bExport.BackColor = oldcolor;
                    bExport.Text = oldtext;
                    return;
                }
            }

            cmd = "mysqldump.exe -t -h" + serverdata.ip + " -u" + serverdata.user + " -p" + serverdata.password + " " + serverdata.database + " --tables " +
                    " function_groups  --where=\"id = '" + stationList[lvAllStations.SelectedIndices[0]].station_id + "'\" > " + dumpfilename;
            if ((status = ExecuteCommand(cmd, 10000, ref outputString, ref errorString)) != 0)
            {
                MessageBox.Show("Error sending station data. " + errorString);
                bExport.BackColor = oldcolor;
                bExport.Text = oldtext;
                return;
            }

            cmd = "mysqldump.exe -t -h" + serverdata.ip + " -u" + serverdata.user + " -p" + serverdata.password + " " + serverdata.database + " --tables " +
                    " product_codes  --where=\"group_link = '" + stationList[lvAllStations.SelectedIndices[0]].station_id + "'\" >> " + dumpfilename;
            if ((status = ExecuteCommand(cmd, 10000, ref outputString, ref errorString)) != 0)
            {
                MessageBox.Show("Error sending product data. " + errorString);
                bExport.BackColor = oldcolor;
                bExport.Text = oldtext;
                return;
            }
            bExport.BackColor = oldcolor;
            bExport.Text = oldtext;

        }

        private void bDirSelector_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Set directory to save SQL files";
            tbDirectory.Text = Properties.Settings.Default.SQLDirectory;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbDirectory.Text = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.SQLDirectory = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }
    }
}