﻿namespace StationEditor
{
    partial class StationExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbExportingFrom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.bExport = new System.Windows.Forms.Button();
            this.lvAllStations = new System.Windows.Forms.ListView();
            this.lvStationName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFunction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvModifyDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvActive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tbDirectory = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bDirSelector = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbExportingFrom
            // 
            this.lbExportingFrom.AutoSize = true;
            this.lbExportingFrom.Location = new System.Drawing.Point(136, 399);
            this.lbExportingFrom.Name = "lbExportingFrom";
            this.lbExportingFrom.Size = new System.Drawing.Size(19, 13);
            this.lbExportingFrom.TabIndex = 21;
            this.lbExportingFrom.Text = "----";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 399);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Exporting From";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(583, 151);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Done";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // bExport
            // 
            this.bExport.Location = new System.Drawing.Point(583, 109);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(75, 23);
            this.bExport.TabIndex = 18;
            this.bExport.Text = "Export";
            this.bExport.UseVisualStyleBackColor = true;
            this.bExport.Click += new System.EventHandler(this.bExport_Click);
            // 
            // lvAllStations
            // 
            this.lvAllStations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvStationName,
            this.lvFunction,
            this.lvModifyDate,
            this.lvActive});
            this.lvAllStations.FullRowSelect = true;
            this.lvAllStations.GridLines = true;
            this.lvAllStations.HideSelection = false;
            this.lvAllStations.Location = new System.Drawing.Point(29, 49);
            this.lvAllStations.Name = "lvAllStations";
            this.lvAllStations.Size = new System.Drawing.Size(514, 334);
            this.lvAllStations.TabIndex = 16;
            this.lvAllStations.UseCompatibleStateImageBehavior = false;
            this.lvAllStations.View = System.Windows.Forms.View.Details;
            // 
            // lvStationName
            // 
            this.lvStationName.Text = "Station Name";
            this.lvStationName.Width = 261;
            // 
            // lvFunction
            // 
            this.lvFunction.Text = "Function";
            this.lvFunction.Width = 67;
            // 
            // lvModifyDate
            // 
            this.lvModifyDate.Text = "Modify Date";
            this.lvModifyDate.Width = 110;
            // 
            // lvActive
            // 
            this.lvActive.Text = "Active";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Current Stations";
            // 
            // tbDirectory
            // 
            this.tbDirectory.Location = new System.Drawing.Point(29, 451);
            this.tbDirectory.Name = "tbDirectory";
            this.tbDirectory.Size = new System.Drawing.Size(401, 20);
            this.tbDirectory.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 432);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Saving SQL to";
            // 
            // bDirSelector
            // 
            this.bDirSelector.Location = new System.Drawing.Point(437, 451);
            this.bDirSelector.Name = "bDirSelector";
            this.bDirSelector.Size = new System.Drawing.Size(27, 23);
            this.bDirSelector.TabIndex = 24;
            this.bDirSelector.Text = "...";
            this.bDirSelector.UseVisualStyleBackColor = true;
            this.bDirSelector.Click += new System.EventHandler(this.bDirSelector_Click);
            // 
            // StationExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 492);
            this.Controls.Add(this.bDirSelector);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbDirectory);
            this.Controls.Add(this.lbExportingFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bExport);
            this.Controls.Add(this.lvAllStations);
            this.Controls.Add(this.label1);
            this.Name = "StationExport";
            this.Text = "StationExport";
            this.Load += new System.EventHandler(this.StationExport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbExportingFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bExport;
        private System.Windows.Forms.ListView lvAllStations;
        private System.Windows.Forms.ColumnHeader lvStationName;
        private System.Windows.Forms.ColumnHeader lvFunction;
        private System.Windows.Forms.ColumnHeader lvModifyDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader lvActive;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tbDirectory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bDirSelector;
    }
}