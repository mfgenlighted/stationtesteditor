﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProductDatabaseDLL;

namespace StationEditor
{
    public partial class SelsectedStationList : Form
    {
        List<ProductDatabase.Product_data> productData = new List<ProductDatabase.Product_data>();
        ProductDatabase.Station_data stationData = new ProductDatabase.Station_data();

        private serverDataItem server = new serverDataItem();
        private string filter = string.Empty;

        public SelsectedStationList(ProductDatabase.Station_data sdata, serverDataItem serverData)
        {
            stationData = sdata;
            server = serverData;
            InitializeComponent();
            lvStationData.ColumnClick += new ColumnClickEventHandler(ColumnClick);
        }

        // ColumnClick event handler.
        private void ColumnClick(object o, ColumnClickEventArgs e)
        {
            // Set the ListViewItemSorter property to a new ListViewItemComparer 
            // object. Setting this property immediately sorts the 
            // ListView using the ListViewItemComparer object.
            this.lvStationData.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void SelsectedStationList_Load(object sender, EventArgs e)
        {
            string msg = string.Empty;

            ProductDatabase prod = new ProductDatabase();
            prod.Open(server.ip, server.database, server.user, server.password);
            prod.GetStationProductData(stationData.name, false, out productData, out msg);   // get active and not active items
            prod.Close();
            if (msg != string.Empty)
            {
                MessageBox.Show("Error getting data. " + msg);
            }
            else
            {
                lbStation.Text = stationData.name;
                lbFunction.Text = stationData.function;

                RewriteList();
            }
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void bEdit_Click(object sender, EventArgs e)
        {
            DialogResult editresponse;
            int selectedProductDataIndex = 0;

            if (lvStationData.SelectedItems.Count == 0)
                return;

            for (int i = 0; i < productData.Count; i++)
            {
                if (lvStationData.SelectedItems[0].Text == productData[i].product_code)
                {
                    selectedProductDataIndex = i;
                }
            }


            EditProductCodeEntry iform = new StationEditor.EditProductCodeEntry(stationData, productData[selectedProductDataIndex]);
           
            iform.ShowDialog();
            productData[lvStationData.SelectedIndices[0]] = iform.productData;
            editresponse = iform.DialogResult;
            iform.Dispose();

            if (editresponse == DialogResult.Cancel)
                return;

            ProductDatabase prod = new ProductDatabase();
            if (!prod.Open(server.ip, server.database, server.user, server.password))
            {
                MessageBox.Show("Problem opening database.");
                return;
            }
            if (!prod.ReplaceStationProductData(productData[lvStationData.SelectedIndices[0]].product_code_id, productData[lvStationData.SelectedIndices[0]]))
            {
                MessageBox.Show("There was a problem adding edited product data. Window may not match database. Refresh window." + prod.LastErrorMessage);
            }
            prod.Close();

            RewriteList();        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            ProductDatabase.Product_data newproductData = new ProductDatabase.Product_data();

            var yn = MessageBox.Show("Do you want to base the new item on a old one?", "Create new item", MessageBoxButtons.YesNo);
            if (yn == DialogResult.Yes)
            {
                PickProductCode pform = new PickProductCode(productData);
                pform.ShowDialog();
                newproductData = pform.productData;
                newproductData.product_code_id = Guid.NewGuid();
                pform.Dispose();
            }
            else
            {
                newproductData.station_id = stationData.station_id;                // link it to current function
                newproductData.product_code_id = stationData.station_id;
                newproductData.product_code = string.Empty;
                newproductData.pcba_product_code = string.Empty;
                newproductData.model = string.Empty;
                newproductData.pn = string.Empty;
                newproductData.pcba_used = string.Empty;
                newproductData.family_code = string.Empty;
                newproductData.product_desc = string.Empty;
                newproductData.active_part = true;
                newproductData.eco = string.Empty;
                newproductData.product_code_modify_date = DateTime.Now;
                newproductData.pfs_assembly_number = string.Empty;
                newproductData.limit_version = string.Empty;
                newproductData.product_code_comment = string.Empty;
                newproductData.cm_codes = "E";
            }

            EditProductCodeEntry iform = new StationEditor.EditProductCodeEntry(stationData, newproductData);
            if (iform.ShowDialog() == DialogResult.OK)
            {
                newproductData = iform.productData;
                productData.Add(newproductData);
                ProductDatabase prod = new ProductDatabase();
                prod.Open(server.ip, server.database, server.user, server.password);
                if (!prod.AddStationProductData(newproductData))
                {
                    MessageBox.Show("There was a problem adding new station." + prod.LastErrorMessage);
                }
                prod.Close();
            }
            iform.Dispose();

            RewriteList();        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            int selectedProductDataIndex = 0;

            for (int i = 0; i < productData.Count; i++)
            {
                if (lvStationData.SelectedItems[0].Text == productData[i].product_code)
                {
                    selectedProductDataIndex = i;
                }
            }


            ProductDatabase prod = new ProductDatabase();
            prod.Open(server.ip, server.database, server.user, server.password);
            if (!prod.ReplaceStationProductData(productData[selectedProductDataIndex].product_code_id, null))
            {
                MessageBox.Show("Error trying to delete. " + prod.LastErrorMessage);
            }
            prod.Close();
            RewriteList();
        }


        private void bRereadProducts_Click(object sender, EventArgs e)
        {
            RewriteList();
        }

        private void RewriteList()
        {
            string message = string.Empty;

            ProductDatabase prod = new ProductDatabase();
            prod.Open(server.ip, server.database, server.user, server.password);
            if (tbFilter.Text == string.Empty)
                prod.GetStationProductData(stationData.name, false, out productData, out message);   // get active and not active items
            else
                prod.GetStationProductData(stationData.name, false, out productData, tbFilter.Text, out message);
            prod.Close();

            lvStationData.Items.Clear();
            foreach (var item in productData)
            {
                if (!cbDisplayActiveOnly.Checked)   // display everything
                {
                    ListViewItem oneItem = new ListViewItem(item.product_code);
                    oneItem.SubItems.Add(item.limit_version);
                    oneItem.SubItems.Add(item.active_part ? "YES" : "NO");
                    oneItem.SubItems.Add(item.product_code_modify_date.ToString());
                    oneItem.SubItems.Add(item.cm_codes);
                    lvStationData.Items.Add(oneItem);
                }
                else        // display only activer
                {
                    if (item.active_part)
                    {
                        ListViewItem oneItem = new ListViewItem(item.product_code);
                        oneItem.SubItems.Add(item.limit_version);
                        oneItem.SubItems.Add(item.active_part ? "YES" : "NO");
                        oneItem.SubItems.Add(item.product_code_modify_date.ToString());
                        oneItem.SubItems.Add(item.cm_codes);
                        lvStationData.Items.Add(oneItem);
                    }

                }
            }

        }
    }
}
