﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProductDatabaseDLL;

namespace StationEditor
{

    public partial class EditStationEntry: Form
    {
        public ProductDatabase.Station_data stationdata;

        public EditStationEntry(ProductDatabase.Station_data data)
        {
            stationdata = data;
            InitializeComponent();
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            stationdata.name = tbName.Text;
            stationdata.function = tbFunction.Text;
            stationdata.active = cbActive.Checked;
            stationdata.description = tbDescription.Text;
            stationdata.allowed_testprogram_version = tbAllowedVersion.Text;
            stationdata.cm_code = tbCMCodes.Text;
            stationdata.modify_date = DateTime.Now;
            stationdata.comment = tbComment.Text;
            DialogResult = DialogResult.Yes;
        }

        private void EditStationEntry_Load(object sender, EventArgs e)
        {
            tbName.Text = stationdata.name;
            tbFunction.Text = stationdata.function;
            cbActive.Checked = stationdata.active;
            tbDescription.Text = stationdata.description;
            tbAllowedVersion.Text = stationdata.allowed_testprogram_version;
            tbCMCodes.Text = stationdata.cm_code;
            tbComment.Text = stationdata.comment;
        }
    }
}
