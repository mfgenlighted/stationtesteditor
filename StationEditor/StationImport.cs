﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using ProductDatabaseDLL;

namespace StationEditor
{
    public partial class StationImport : Form
    {
        serverDataItem serverdata = new serverDataItem();

        public StationImport()
        {
            InitializeComponent();
        }

        private void StationImport_Load(object sender, EventArgs e)
        {

            SelectServerToOpen iform = new StationEditor.SelectServerToOpen();
            iform.ShowDialog();
            lImportingTo.Text = iform.SelectedServer;
            serverdata = iform.selectedServer;
            iform.Dispose();

        }

        private static int ExecuteCommand(string commnd, int timeout, ref string outputString, ref string errorString)
        {

            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C:\\",
            };
            pp.RedirectStandardError = true;
            pp.RedirectStandardOutput = true;
            var process = Process.Start(pp);
            try
            {
                process.WaitForExit(timeout);
            }
            catch (Exception ex)
            {
                errorString = "Exception rasied. " + ex.Message;
                return -1;
            }
            using (StreamReader rdr = process.StandardOutput)
            {
                outputString = rdr.ReadToEnd();
            }
            using (StreamReader rdr = process.StandardError)
            {
                errorString = rdr.ReadToEnd();
            }
            process.Close();
            return 0;
        }


        private void SelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select file to import.";
            openFileDialog1.InitialDirectory = Properties.Settings.Default.SQLDirectory;
            openFileDialog1.Filter = "SQL files |*.sql";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                tbFile.Text = "\"" + openFileDialog1.FileName + "\"";     // put ' in just in case there are spaces

        }

        private void bStartImport_Click(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            string outputString = string.Empty;
            string errorString = string.Empty;
            int errorCode = 0;
            string dupID = string.Empty;
            Color oldcolor;
            string oldtext;
            ProductDatabaseDLL.ProductDatabase prodDB = new ProductDatabaseDLL.ProductDatabase();
            List<ProductDatabaseDLL.ProductDatabase.Station_data> stationList = new List<ProductDatabaseDLL.ProductDatabase.Station_data>();

            oldcolor = bStartImport.BackColor;
            bStartImport.BackColor = Color.Green;
            oldtext = bStartImport.Text;
            bStartImport.Text = "Working";

            cmd = "mysql -t -h" + serverdata.ip + " -u" + serverdata.user + " -p" + serverdata.password + " " + serverdata.database + " < " + tbFile.Text;
            if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
            {
                MessageBox.Show("Error executing command. " + errorString);
                return;
            }
            if (errorString != null)
            { 
                // see if the error is a duplicate
                if (errorString.Contains("Duplicate entry"))
                {
                    // ask if they are sure that they want to replace
                    if (MessageBox.Show("There is a station with that info currently in the server. Do you want to replace it with the new data?", "Duplicate Station", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // to replace, delete old and import new
                        string[] parts = errorString.Split('\'');       // [1] has the function_groups id
                        if (!prodDB.Open(serverdata.ip, serverdata.database, serverdata.user, serverdata.password))
                        {
                            MessageBox.Show("Error opening the database. ");
                            return;
                        }
                        Guid g = new Guid(parts[1]);
                        if (!prodDB.ReplaceStationData(g, null))       // if there was a error
                        {
                            prodDB.Close();
                            errorCode = prodDB.CheckForErrors(out outputString);
                            MessageBox.Show("Error deleting data. Error code = " + errorCode + " " + outputString);
                            return;
                        }
                        prodDB.Close();
                        // try the import again
                        if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                        {
                            MessageBox.Show("Error executing command. " + errorString);
                            return;
                        }
                        if (errorString != string.Empty)
                        {
                            if (!errorString.Contains("[Warning]" ))
                            {
                                MessageBox.Show("There is a problem trying to import the data. " + errorString);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Station on server has not been updated.");
                        return;
                    }

                }
            }
            bStartImport.BackColor = oldcolor;
            bStartImport.Text = oldtext;

        }
    }
}
