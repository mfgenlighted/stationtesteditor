﻿namespace StationEditor
{
    partial class SelsectedStationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvStationData = new System.Windows.Forms.ListView();
            this.chProductCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbLimitVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chActive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chModifyDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvCMCodes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbStation = new System.Windows.Forms.Label();
            this.lbFunction = new System.Windows.Forms.Label();
            this.bAdd = new System.Windows.Forms.Button();
            this.bEdit = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bDone = new System.Windows.Forms.Button();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bRereadProducts = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cbDisplayActiveOnly = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lvStationData
            // 
            this.lvStationData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chProductCode,
            this.cbLimitVersion,
            this.chActive,
            this.chModifyDate,
            this.lvCMCodes});
            this.lvStationData.HideSelection = false;
            this.lvStationData.Location = new System.Drawing.Point(65, 65);
            this.lvStationData.Name = "lvStationData";
            this.lvStationData.Size = new System.Drawing.Size(537, 529);
            this.lvStationData.TabIndex = 0;
            this.lvStationData.UseCompatibleStateImageBehavior = false;
            this.lvStationData.View = System.Windows.Forms.View.Details;
            // 
            // chProductCode
            // 
            this.chProductCode.Text = "Product Code";
            this.chProductCode.Width = 98;
            // 
            // cbLimitVersion
            // 
            this.cbLimitVersion.Text = "Limit Version";
            this.cbLimitVersion.Width = 148;
            // 
            // chActive
            // 
            this.chActive.Text = "Active";
            // 
            // chModifyDate
            // 
            this.chModifyDate.Text = "ModifyDate";
            this.chModifyDate.Width = 132;
            // 
            // lvCMCodes
            // 
            this.lvCMCodes.Text = "CM Codes";
            this.lvCMCodes.Width = 66;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Station";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Function";
            // 
            // lbStation
            // 
            this.lbStation.AutoSize = true;
            this.lbStation.Location = new System.Drawing.Point(91, 25);
            this.lbStation.Name = "lbStation";
            this.lbStation.Size = new System.Drawing.Size(35, 13);
            this.lbStation.TabIndex = 3;
            this.lbStation.Text = "label3";
            // 
            // lbFunction
            // 
            this.lbFunction.AutoSize = true;
            this.lbFunction.Location = new System.Drawing.Point(94, 49);
            this.lbFunction.Name = "lbFunction";
            this.lbFunction.Size = new System.Drawing.Size(35, 13);
            this.lbFunction.TabIndex = 4;
            this.lbFunction.Text = "label3";
            // 
            // bAdd
            // 
            this.bAdd.Location = new System.Drawing.Point(619, 107);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(75, 23);
            this.bAdd.TabIndex = 5;
            this.bAdd.Text = "Add";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bEdit
            // 
            this.bEdit.Location = new System.Drawing.Point(619, 153);
            this.bEdit.Name = "bEdit";
            this.bEdit.Size = new System.Drawing.Size(75, 23);
            this.bEdit.TabIndex = 6;
            this.bEdit.Text = "Edit";
            this.bEdit.UseVisualStyleBackColor = true;
            this.bEdit.Click += new System.EventHandler(this.bEdit_Click);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(619, 199);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 7;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(619, 285);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 8;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // tbFilter
            // 
            this.tbFilter.Location = new System.Drawing.Point(106, 616);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(100, 20);
            this.tbFilter.TabIndex = 9;
            this.toolTip1.SetToolTip(this.tbFilter, "Will filter the product list looking for products that have this string as any pa" +
        "rt of the name");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 616);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Filter";
            // 
            // bRereadProducts
            // 
            this.bRereadProducts.Location = new System.Drawing.Point(226, 616);
            this.bRereadProducts.Name = "bRereadProducts";
            this.bRereadProducts.Size = new System.Drawing.Size(107, 23);
            this.bRereadProducts.TabIndex = 11;
            this.bRereadProducts.Text = "Reread products";
            this.bRereadProducts.UseVisualStyleBackColor = true;
            this.bRereadProducts.Click += new System.EventHandler(this.bRereadProducts_Click);
            // 
            // cbDisplayActiveOnly
            // 
            this.cbDisplayActiveOnly.AutoSize = true;
            this.cbDisplayActiveOnly.Checked = true;
            this.cbDisplayActiveOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDisplayActiveOnly.Location = new System.Drawing.Point(389, 622);
            this.cbDisplayActiveOnly.Name = "cbDisplayActiveOnly";
            this.cbDisplayActiveOnly.Size = new System.Drawing.Size(117, 17);
            this.cbDisplayActiveOnly.TabIndex = 12;
            this.cbDisplayActiveOnly.Text = "Display Active Only";
            this.cbDisplayActiveOnly.UseVisualStyleBackColor = true;
            // 
            // SelsectedStationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 675);
            this.ControlBox = false;
            this.Controls.Add(this.cbDisplayActiveOnly);
            this.Controls.Add(this.bRereadProducts);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbFilter);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bEdit);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.lbFunction);
            this.Controls.Add(this.lbStation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvStationData);
            this.Name = "SelsectedStationList";
            this.Text = "SelsectedStationList";
            this.Load += new System.EventHandler(this.SelsectedStationList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvStationData;
        private System.Windows.Forms.ColumnHeader chProductCode;
        private System.Windows.Forms.ColumnHeader cbLimitVersion;
        private System.Windows.Forms.ColumnHeader chActive;
        private System.Windows.Forms.ColumnHeader chModifyDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbStation;
        private System.Windows.Forms.Label lbFunction;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Button bEdit;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.ColumnHeader lvCMCodes;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bRereadProducts;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox cbDisplayActiveOnly;
    }
}