﻿namespace StationEditor
{
    partial class EditProductCodeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tbHLAPN = new System.Windows.Forms.TextBox();
            this.tbPCBAPN = new System.Windows.Forms.TextBox();
            this.tbFamilyCode = new System.Windows.Forms.TextBox();
            this.tbProductDesc = new System.Windows.Forms.TextBox();
            this.tbPFSAssemblyNumber = new System.Windows.Forms.TextBox();
            this.tbECO = new System.Windows.Forms.TextBox();
            this.tbModifyDate = new System.Windows.Forms.TextBox();
            this.tbPCBAProductCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbActivePart = new System.Windows.Forms.CheckBox();
            this.tbLimitVersion = new System.Windows.Forms.TextBox();
            this.tbComment = new System.Windows.Forms.TextBox();
            this.bSave = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbHLAProductCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lbStation = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbFunction = new System.Windows.Forms.Label();
            this.tbCMcodes = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudNumberOfMacs = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.tbFirmwareVersion = new System.Windows.Forms.TextBox();
            this.tbFirmwareID = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.nudHwCode = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfMacs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHwCode)).BeginInit();
            this.SuspendLayout();
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(278, 152);
            this.tbModel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbModel.MaxLength = 50;
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(242, 26);
            this.tbModel.TabIndex = 2;
            // 
            // tbHLAPN
            // 
            this.tbHLAPN.Location = new System.Drawing.Point(278, 189);
            this.tbHLAPN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHLAPN.MaxLength = 15;
            this.tbHLAPN.Name = "tbHLAPN";
            this.tbHLAPN.Size = new System.Drawing.Size(242, 26);
            this.tbHLAPN.TabIndex = 3;
            // 
            // tbPCBAPN
            // 
            this.tbPCBAPN.Location = new System.Drawing.Point(278, 226);
            this.tbPCBAPN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPCBAPN.MaxLength = 15;
            this.tbPCBAPN.Name = "tbPCBAPN";
            this.tbPCBAPN.Size = new System.Drawing.Size(242, 26);
            this.tbPCBAPN.TabIndex = 4;
            // 
            // tbFamilyCode
            // 
            this.tbFamilyCode.Location = new System.Drawing.Point(278, 263);
            this.tbFamilyCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbFamilyCode.MaxLength = 20;
            this.tbFamilyCode.Name = "tbFamilyCode";
            this.tbFamilyCode.Size = new System.Drawing.Size(242, 26);
            this.tbFamilyCode.TabIndex = 5;
            // 
            // tbProductDesc
            // 
            this.tbProductDesc.Location = new System.Drawing.Point(278, 332);
            this.tbProductDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbProductDesc.MaxLength = 255;
            this.tbProductDesc.Name = "tbProductDesc";
            this.tbProductDesc.Size = new System.Drawing.Size(377, 26);
            this.tbProductDesc.TabIndex = 6;
            // 
            // tbPFSAssemblyNumber
            // 
            this.tbPFSAssemblyNumber.Location = new System.Drawing.Point(278, 443);
            this.tbPFSAssemblyNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPFSAssemblyNumber.MaxLength = 40;
            this.tbPFSAssemblyNumber.Name = "tbPFSAssemblyNumber";
            this.tbPFSAssemblyNumber.Size = new System.Drawing.Size(242, 26);
            this.tbPFSAssemblyNumber.TabIndex = 9;
            // 
            // tbECO
            // 
            this.tbECO.Location = new System.Drawing.Point(282, 582);
            this.tbECO.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbECO.MaxLength = 20;
            this.tbECO.Name = "tbECO";
            this.tbECO.Size = new System.Drawing.Size(242, 26);
            this.tbECO.TabIndex = 10;
            // 
            // tbModifyDate
            // 
            this.tbModifyDate.Location = new System.Drawing.Point(282, 619);
            this.tbModifyDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbModifyDate.Name = "tbModifyDate";
            this.tbModifyDate.Size = new System.Drawing.Size(242, 26);
            this.tbModifyDate.TabIndex = 11;
            // 
            // tbPCBAProductCode
            // 
            this.tbPCBAProductCode.Location = new System.Drawing.Point(278, 115);
            this.tbPCBAProductCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPCBAProductCode.MaxLength = 5;
            this.tbPCBAProductCode.Name = "tbPCBAProductCode";
            this.tbPCBAProductCode.Size = new System.Drawing.Size(242, 26);
            this.tbPCBAProductCode.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(534, 231);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.TabIndex = 16;
            this.label2.Text = "PCBA PN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(538, 623);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Modify Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(541, 700);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Comment";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(663, 338);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Product Desc";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(534, 194);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "HLA PN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(534, 120);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 20);
            this.label9.TabIndex = 23;
            this.label9.Text = "PCBA Product code";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(538, 586);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 20);
            this.label12.TabIndex = 26;
            this.label12.Text = "ECO";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(534, 447);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(172, 20);
            this.label13.TabIndex = 27;
            this.label13.Text = "PFS Assembly Number";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(663, 409);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 28;
            this.label14.Text = "Limit Version";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(534, 268);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "Family Code";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(534, 157);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 20);
            this.label16.TabIndex = 30;
            this.label16.Text = "Model";
            // 
            // cbActivePart
            // 
            this.cbActivePart.AutoSize = true;
            this.cbActivePart.Location = new System.Drawing.Point(501, 370);
            this.cbActivePart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbActivePart.Name = "cbActivePart";
            this.cbActivePart.Size = new System.Drawing.Size(111, 24);
            this.cbActivePart.TabIndex = 7;
            this.cbActivePart.Text = "Active Part";
            this.cbActivePart.UseVisualStyleBackColor = true;
            // 
            // tbLimitVersion
            // 
            this.tbLimitVersion.Location = new System.Drawing.Point(282, 406);
            this.tbLimitVersion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbLimitVersion.MaxLength = 255;
            this.tbLimitVersion.Name = "tbLimitVersion";
            this.tbLimitVersion.Size = new System.Drawing.Size(373, 26);
            this.tbLimitVersion.TabIndex = 8;
            // 
            // tbComment
            // 
            this.tbComment.Location = new System.Drawing.Point(61, 695);
            this.tbComment.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbComment.MaxLength = 255;
            this.tbComment.Multiline = true;
            this.tbComment.Name = "tbComment";
            this.tbComment.Size = new System.Drawing.Size(463, 90);
            this.tbComment.TabIndex = 12;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(57, 63);
            this.bSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(112, 35);
            this.bSave.TabIndex = 13;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(57, 109);
            this.bCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(112, 35);
            this.bCancel.TabIndex = 14;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(534, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "HLA Product code";
            // 
            // tbHLAProductCode
            // 
            this.tbHLAProductCode.Location = new System.Drawing.Point(278, 75);
            this.tbHLAProductCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHLAProductCode.MaxLength = 5;
            this.tbHLAProductCode.Name = "tbHLAProductCode";
            this.tbHLAProductCode.Size = new System.Drawing.Size(242, 26);
            this.tbHLAProductCode.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 20);
            this.label5.TabIndex = 40;
            this.label5.Text = "Station:";
            // 
            // lbStation
            // 
            this.lbStation.AutoSize = true;
            this.lbStation.Location = new System.Drawing.Point(332, 6);
            this.lbStation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbStation.Name = "lbStation";
            this.lbStation.Size = new System.Drawing.Size(72, 20);
            this.lbStation.TabIndex = 41;
            this.lbStation.Text = "lbStation";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(501, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 20);
            this.label10.TabIndex = 42;
            this.label10.Text = "Function:";
            // 
            // lbFunction
            // 
            this.lbFunction.AutoSize = true;
            this.lbFunction.Location = new System.Drawing.Point(580, 5);
            this.lbFunction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbFunction.Name = "lbFunction";
            this.lbFunction.Size = new System.Drawing.Size(83, 20);
            this.lbFunction.TabIndex = 43;
            this.lbFunction.Text = "lbFunction";
            // 
            // tbCMcodes
            // 
            this.tbCMcodes.Location = new System.Drawing.Point(282, 659);
            this.tbCMcodes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCMcodes.MaxLength = 11;
            this.tbCMcodes.Name = "tbCMcodes";
            this.tbCMcodes.Size = new System.Drawing.Size(242, 26);
            this.tbCMcodes.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(542, 659);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "CM Codes";
            // 
            // nudNumberOfMacs
            // 
            this.nudNumberOfMacs.Location = new System.Drawing.Point(282, 298);
            this.nudNumberOfMacs.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudNumberOfMacs.Name = "nudNumberOfMacs";
            this.nudNumberOfMacs.Size = new System.Drawing.Size(120, 26);
            this.nudNumberOfMacs.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(534, 300);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 20);
            this.label11.TabIndex = 47;
            this.label11.Text = "Number of Macs";
            // 
            // tbFirmwareVersion
            // 
            this.tbFirmwareVersion.Location = new System.Drawing.Point(278, 477);
            this.tbFirmwareVersion.MaxLength = 255;
            this.tbFirmwareVersion.Name = "tbFirmwareVersion";
            this.tbFirmwareVersion.Size = new System.Drawing.Size(174, 26);
            this.tbFirmwareVersion.TabIndex = 48;
            // 
            // tbFirmwareID
            // 
            this.tbFirmwareID.Location = new System.Drawing.Point(278, 514);
            this.tbFirmwareID.MaxLength = 30;
            this.tbFirmwareID.Name = "tbFirmwareID";
            this.tbFirmwareID.Size = new System.Drawing.Size(174, 26);
            this.tbFirmwareID.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(534, 483);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(229, 20);
            this.label17.TabIndex = 50;
            this.label17.Text = "Firmware Version/PCBA Printer";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(534, 517);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(204, 20);
            this.label18.TabIndex = 51;
            this.label18.Text = "Firmware ID/Product Printer";
            // 
            // nudHwCode
            // 
            this.nudHwCode.Location = new System.Drawing.Point(278, 548);
            this.nudHwCode.Name = "nudHwCode";
            this.nudHwCode.Size = new System.Drawing.Size(120, 26);
            this.nudHwCode.TabIndex = 52;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(534, 550);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(199, 20);
            this.label19.TabIndex = 53;
            this.label19.Text = "Hardware Code/Scan Type";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(754, 550);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 20);
            this.label20.TabIndex = 54;
            this.label20.Text = "0-Read, 1-Scan";
            // 
            // EditProductCodeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 812);
            this.ControlBox = false;
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.nudHwCode);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbFirmwareID);
            this.Controls.Add(this.tbFirmwareVersion);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.nudNumberOfMacs);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbCMcodes);
            this.Controls.Add(this.lbFunction);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbStation);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbHLAProductCode);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.tbComment);
            this.Controls.Add(this.tbLimitVersion);
            this.Controls.Add(this.cbActivePart);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPCBAProductCode);
            this.Controls.Add(this.tbModifyDate);
            this.Controls.Add(this.tbECO);
            this.Controls.Add(this.tbPFSAssemblyNumber);
            this.Controls.Add(this.tbProductDesc);
            this.Controls.Add(this.tbFamilyCode);
            this.Controls.Add(this.tbPCBAPN);
            this.Controls.Add(this.tbHLAPN);
            this.Controls.Add(this.tbModel);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EditProductCodeEntry";
            this.Text = "Edit Product Code Entry";
            this.Load += new System.EventHandler(this.EditStationEntry_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfMacs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHwCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TextBox tbHLAPN;
        private System.Windows.Forms.TextBox tbPCBAPN;
        private System.Windows.Forms.TextBox tbFamilyCode;
        private System.Windows.Forms.TextBox tbProductDesc;
        private System.Windows.Forms.TextBox tbPFSAssemblyNumber;
        private System.Windows.Forms.TextBox tbECO;
        private System.Windows.Forms.TextBox tbModifyDate;
        private System.Windows.Forms.TextBox tbPCBAProductCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox cbActivePart;
        private System.Windows.Forms.TextBox tbLimitVersion;
        private System.Windows.Forms.TextBox tbComment;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHLAProductCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbStation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbFunction;
        private System.Windows.Forms.TextBox tbCMcodes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudNumberOfMacs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbFirmwareVersion;
        private System.Windows.Forms.TextBox tbFirmwareID;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nudHwCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}