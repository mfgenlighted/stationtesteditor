﻿namespace StationEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serversToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bReadStationData = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbSelectedServer = new System.Windows.Forms.Label();
            this.bAddNewStation = new System.Windows.Forms.Button();
            this.lvStations = new System.Windows.Forms.ListView();
            this.lvStation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFunction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvActive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvCMCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bReplaceData = new System.Windows.Forms.Button();
            this.bEditProductCodes = new System.Windows.Forms.Button();
            this.bEditStationData = new System.Windows.Forms.Button();
            this.bDeleteStation = new System.Windows.Forms.Button();
            this.bExportStationData = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.serversToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(853, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.exitToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(104, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // serversToolStripMenuItem
            // 
            this.serversToolStripMenuItem.Name = "serversToolStripMenuItem";
            this.serversToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.serversToolStripMenuItem.Text = "Servers";
            this.serversToolStripMenuItem.Click += new System.EventHandler(this.serversToolStripMenuItem_Click);
            // 
            // bReadStationData
            // 
            this.bReadStationData.Location = new System.Drawing.Point(677, 60);
            this.bReadStationData.Name = "bReadStationData";
            this.bReadStationData.Size = new System.Drawing.Size(107, 23);
            this.bReadStationData.TabIndex = 2;
            this.bReadStationData.Text = "Read Station Data";
            this.bReadStationData.UseVisualStyleBackColor = true;
            this.bReadStationData.Click += new System.EventHandler(this.bReadStationData_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(674, 331);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selected Server";
            // 
            // lbSelectedServer
            // 
            this.lbSelectedServer.AutoSize = true;
            this.lbSelectedServer.Location = new System.Drawing.Point(677, 348);
            this.lbSelectedServer.Name = "lbSelectedServer";
            this.lbSelectedServer.Size = new System.Drawing.Size(31, 13);
            this.lbSelectedServer.TabIndex = 4;
            this.lbSelectedServer.Text = "none";
            // 
            // bAddNewStation
            // 
            this.bAddNewStation.Location = new System.Drawing.Point(677, 102);
            this.bAddNewStation.Name = "bAddNewStation";
            this.bAddNewStation.Size = new System.Drawing.Size(93, 23);
            this.bAddNewStation.TabIndex = 5;
            this.bAddNewStation.Text = "Add new station";
            this.bAddNewStation.UseVisualStyleBackColor = true;
            this.bAddNewStation.Click += new System.EventHandler(this.bAddNewStation_Click);
            // 
            // lvStations
            // 
            this.lvStations.AllowColumnReorder = true;
            this.lvStations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvStation,
            this.lvFunction,
            this.lvActive,
            this.lvCMCode});
            this.lvStations.FullRowSelect = true;
            this.lvStations.HideSelection = false;
            this.lvStations.LabelEdit = true;
            this.lvStations.Location = new System.Drawing.Point(34, 27);
            this.lvStations.Name = "lvStations";
            this.lvStations.Size = new System.Drawing.Size(579, 345);
            this.lvStations.TabIndex = 8;
            this.lvStations.UseCompatibleStateImageBehavior = false;
            this.lvStations.View = System.Windows.Forms.View.Details;
            // 
            // lvStation
            // 
            this.lvStation.Text = "Station";
            this.lvStation.Width = 316;
            // 
            // lvFunction
            // 
            this.lvFunction.Text = "Function";
            this.lvFunction.Width = 104;
            // 
            // lvActive
            // 
            this.lvActive.Text = "Active";
            // 
            // lvCMCode
            // 
            this.lvCMCode.Text = "CM Code";
            this.lvCMCode.Width = 79;
            // 
            // bReplaceData
            // 
            this.bReplaceData.Location = new System.Drawing.Point(677, 254);
            this.bReplaceData.Name = "bReplaceData";
            this.bReplaceData.Size = new System.Drawing.Size(125, 23);
            this.bReplaceData.TabIndex = 9;
            this.bReplaceData.Text = "Replace station data";
            this.bReplaceData.UseVisualStyleBackColor = true;
            this.bReplaceData.Click += new System.EventHandler(this.bReplaceData_Click);
            // 
            // bEditProductCodes
            // 
            this.bEditProductCodes.Location = new System.Drawing.Point(677, 201);
            this.bEditProductCodes.Name = "bEditProductCodes";
            this.bEditProductCodes.Size = new System.Drawing.Size(125, 23);
            this.bEditProductCodes.TabIndex = 10;
            this.bEditProductCodes.Text = "Edit Product Codes";
            this.bEditProductCodes.UseVisualStyleBackColor = true;
            this.bEditProductCodes.Click += new System.EventHandler(this.bEditProductCodes_Click);
            // 
            // bEditStationData
            // 
            this.bEditStationData.Location = new System.Drawing.Point(677, 160);
            this.bEditStationData.Name = "bEditStationData";
            this.bEditStationData.Size = new System.Drawing.Size(104, 23);
            this.bEditStationData.TabIndex = 11;
            this.bEditStationData.Text = "Edit Station Data";
            this.bEditStationData.UseVisualStyleBackColor = true;
            this.bEditStationData.Click += new System.EventHandler(this.bEditStationData_Click);
            // 
            // bDeleteStation
            // 
            this.bDeleteStation.Location = new System.Drawing.Point(677, 131);
            this.bDeleteStation.Name = "bDeleteStation";
            this.bDeleteStation.Size = new System.Drawing.Size(93, 23);
            this.bDeleteStation.TabIndex = 12;
            this.bDeleteStation.Text = "Delete a Station";
            this.bDeleteStation.UseVisualStyleBackColor = true;
            this.bDeleteStation.Click += new System.EventHandler(this.bDeleteStation_Click);
            // 
            // bExportStationData
            // 
            this.bExportStationData.Location = new System.Drawing.Point(677, 283);
            this.bExportStationData.Name = "bExportStationData";
            this.bExportStationData.Size = new System.Drawing.Size(125, 23);
            this.bExportStationData.TabIndex = 13;
            this.bExportStationData.Text = "Export station data";
            this.bExportStationData.UseVisualStyleBackColor = true;
            this.bExportStationData.Click += new System.EventHandler(this.bExportStationData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 384);
            this.Controls.Add(this.bExportStationData);
            this.Controls.Add(this.bDeleteStation);
            this.Controls.Add(this.bEditStationData);
            this.Controls.Add(this.bEditProductCodes);
            this.Controls.Add(this.bReplaceData);
            this.Controls.Add(this.lvStations);
            this.Controls.Add(this.bAddNewStation);
            this.Controls.Add(this.lbSelectedServer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bReadStationData);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Station Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serversToolStripMenuItem;
        private System.Windows.Forms.Button bReadStationData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbSelectedServer;
        private System.Windows.Forms.Button bAddNewStation;
        private System.Windows.Forms.ListView lvStations;
        private System.Windows.Forms.ColumnHeader lvStation;
        private System.Windows.Forms.ColumnHeader lvFunction;
        private System.Windows.Forms.ColumnHeader lvActive;
        private System.Windows.Forms.Button bReplaceData;
        private System.Windows.Forms.Button bEditProductCodes;
        private System.Windows.Forms.Button bEditStationData;
        private System.Windows.Forms.Button bDeleteStation;
        private System.Windows.Forms.Button bExportStationData;
        private System.Windows.Forms.ColumnHeader lvCMCode;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

